# docker build -t my_docker_flask:latest .
# docker run -d -p 5000:5000 my_docker_flask:latest
FROM python:latest
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 5000
ENTRYPOINT ["python"]
CMD ["app.py"]


