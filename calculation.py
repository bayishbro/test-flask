from functions import *

def call_result(some_rule, num):

    for i in some_rule:
        number = num[0]
        num.clear()

        if 'a' == i: num.append(fun_a(number))
        elif 'b' == i: num.append(fun_b(number))
        elif 'c' == i: num.append(fun_c(number))
        elif 'd' == i: num.append(fun_d(number))
        elif 'e' == i: num.append(fun_e(number))
        elif 'f' == i: num.append(fun_f(number))

    return num[0]