from flask import Flask
from flask import request
from functions import *
from calculation import *

app = Flask(__name__)


@app.route('/')
def index():
    test = {'data': [1, 2, 3, 4, 5], 'rule': ['a', 'b', 'c', 'd', 'e', 'f']}
    result = []

    for i in test['data']:
        result.append(fun_f(fun_e(fun_d(fun_c(fun_b(fun_a(i)))))))

    return {'result': result}


@app.route('/test/')
def test():
    test = {'data': [5, 3, 2, 4, 4, 9, 1],
            'rule': ['f', 'c', 'a', 'd', 'e', 'f']}
    result = []

    for i in test['data']:
        result.append(call_result(test['rule'], [i]))

    return {'result': result}


"""       JSON test for the /start/ API

{"data": [1, 2, 3, 4, 5], "rule": ["a", "b", "c", "d", "e", "f"]}

        I did't understand 1 point in the task and
        used the Postman
                                            """


@app.route('/start/', methods=['POST'])
def start():
    rule = request.json['rule']
    data = request.json['data']
    result = []

    for i in data:
        result.append(call_result(rule, [i]))

    return {'result': result}


if __name__ == "__main__":
    app.run(host='0.0.0.0', port='5000', debug=True)
